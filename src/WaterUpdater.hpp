#ifndef WATER_LAUNCHER
#define WATER_LAUNCHER

//  Include this to avoir a curl warning
#include "winsock2.h"
#include "wx/wxprec.h"

#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif

#include "wx/protocol/http.h"
#include "wx/wfstream.h"
#include "wx/datstrm.h"
#include "wx/event.h"
#include "wx/thread.h"
#include "wx/zipstrm.h"
#include "wx/url.h"
#include "curl/curl.h"
#include <iostream>
#include <fstream>
#include <vector>

enum {
    ID_PlayWater,
    ID_CheckForUpdates,
    ID_Exit
};

void DownloadFile( const wxString& file, const wxString& out, wxGauge* progress, bool binary );
size_t Writer( void* ptr, size_t sz, size_t nmemb, FILE* stream );
size_t Reader( void* ptr, size_t sz, size_t nmemb, FILE* stream );
int Progress( wxGauge* progress, double t, double d, double ultotal, double ulnow );

class MyFrame;

class DownloadThread : public wxThread
{
    public:
        DownloadThread( MyFrame* h );

        void OnExit();

protected :
        virtual void* Entry();
        MyFrame* handler;
};

class MyFrame: public wxFrame
{
    public:
        MyFrame( wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE );

        void SetRange( int val );
        void SetProgress( int val );
        void SetInfo( const wxString& msg );
        void SetChangelog( const wxString& msg );

        DownloadThread* thread;
        wxCriticalSection threadCS;
        CURL* curl;
        wxButton* checkForUpdates;
        wxButton* playWater;
        bool ready;

    private:
        void OnQuit( wxCommandEvent& event );
        void OnCloseWindow( wxCloseEvent& event );
        void OnPlayWater( wxCommandEvent& event );
        void OnCheckForUpdates( wxCommandEvent& event );

        wxBoxSizer* vertLayout;
        wxGauge* progress;
        wxStaticText* info;
        wxTextCtrl* changelog;




        DECLARE_EVENT_TABLE()
};


class MyApp: public wxApp
{
    public:
        MyApp();

        virtual bool OnInit();
};

#endif
