#include "WaterUpdater.hpp"

size_t Writer( void *ptr, size_t sz, size_t nmemb, FILE* stream ) {
    return fwrite( ptr, sz, nmemb, stream );
}

size_t Reader( void *ptr, size_t sz, size_t nmemb, FILE* stream ) {
    return fread( ptr, sz, nmemb, stream );
}

int Progress( wxGauge *progress, double t, double d, double ultotal, double ulnow ) {
    wxMutexGuiEnter();
    progress->SetValue( d * 100.f / t );
    wxMutexGuiLeave();
    return 0;
}

DownloadThread::DownloadThread( MyFrame* h ) : wxThread( wxTHREAD_DETACHED ) {
    handler = h;
}

void DownloadThread::OnExit() {
    wxCriticalSectionLocker locker( handler->threadCS );
    handler->thread = NULL;
}

wxString GetWXString( const std::string& s ) {
    wxChar cs[s.size() + 1];
    for( int i = 0; i < s.size(); ++i )
        cs[i] = s[i];
    cs[s.size()] = '\0';
    return wxString(cs);
}

std::string GetStdString( const wxString& str ) {
    std::string s;
    for( int i = 0; i < str.size(); ++i ) {
        s.push_back( str[i] );
    }
    return s;
}

//  I don't know why but after downloading, files may have a wrong "Newline" format so we need to put a standard format
std::string ReadFileReformat( const std::string& file ) {
    std::ifstream is( file.c_str() );
    std::string s;
    if( is.is_open() ) {
        std::vector< std::string > lines;
        std::string buffer;

        while( std::getline( is, buffer ) ) {
            buffer.push_back( '\n' );
            lines.push_back( buffer );
        }
        for( int i = 0; i < lines.size(); ++i ) {
            s += lines[i];
        }
        is.close();
    }
    return s;
}

void* DownloadThread::Entry() {
    //  Download version file
    wxMutexGuiEnter();
    handler->SetInfo( wxT("Checking version...") );
    wxMutexGuiLeave();


    std::ifstream is( "version" );
    std::string localVersion( "0.0.0" );
    short localMaj = 0;
    short localMin = 0;
    short localRev = 0;
    std::string version;
    short maj = 0;
    short min = 0;
    short rev = 0;
    if( is.is_open() ) {
        is >> localVersion;
        localMaj = localVersion[0];
        localMin = localVersion[2];
        localRev = localVersion[4];
        is.close();
    }

    FILE* f = fopen( "version", "w+" );
    curl_easy_setopt( handler->curl, CURLOPT_URL, "http://water.olympe.in/files/version" );
    curl_easy_setopt( handler->curl, CURLOPT_WRITEDATA, f );
    curl_easy_perform( handler->curl );
    fclose( f );

    is.open( "version" );
    if( is.is_open() ) {
        is >> version;
        maj = version[0];
        min = version[2];
        rev = version[4];
        is.close();
    }

    if( maj > localMaj || min > localMin || rev > localRev ) {
        //  Download changelog
        wxMutexGuiEnter();
        handler->SetInfo( wxT("Downloading changelog...") );
        wxMutexGuiLeave();
        f = fopen( "changelog", "w+" );
        curl_easy_setopt( handler->curl, CURLOPT_URL, "http://water.olympe.in/files/changelog" );
        curl_easy_setopt( handler->curl, CURLOPT_WRITEDATA, f );
        curl_easy_perform( handler->curl );
        fclose( f );

        std::string log = ReadFileReformat( "changelog" );
        //std::cout << s << std::endl;

        wxMutexGuiEnter();
        handler->SetChangelog( GetWXString( log ) );
        handler->SetInfo( GetWXString(std::string( "Updating from " + localVersion + " to " + version + "..." )) );
        wxMutexGuiLeave();

        //  Download game files
        f = fopen( "bin.zip", "wb+" );
        curl_easy_setopt( handler->curl, CURLOPT_URL, "http://water.olympe.in/files/bin.zip" );
        curl_easy_setopt( handler->curl, CURLOPT_WRITEDATA, f );
        curl_easy_perform( handler->curl );
        fclose( f );

        wxMutexGuiEnter();
        handler->SetInfo( wxT("Done. Extracting files...") );
        wxMutexGuiLeave();

        //  Extract game files
        wxFileInputStream is( wxT("bin.zip") );

        wxZipInputStream zip1( is );
        int totalNbr = zip1.GetTotalEntries();
        int currentNbr = 0;
        //  First loop to create directories
        for( wxZipEntry* entry = zip1.GetNextEntry(); entry != NULL; entry = zip1.GetNextEntry() ) {
            if( entry->IsDir() ) {
                mkdir( GetStdString( entry->GetName() ).c_str() );
                currentNbr++;
                wxMutexGuiEnter();
                handler->SetProgress( currentNbr * 100 / totalNbr );
                wxMutexGuiLeave();
            }
        }
        wxZipInputStream zip2( is );
        //  Second loop to extract files
        for( wxZipEntry* entry = zip2.GetNextEntry(); entry != NULL; entry = zip2.GetNextEntry() ) {
            if( !entry->IsDir() ) {
                wxFileOutputStream os( entry->GetName() );
                zip2.Read( os );
                currentNbr++;
                wxMutexGuiEnter();
                handler->SetProgress( currentNbr * 100 / totalNbr );
                wxMutexGuiLeave();
            }
        }
        wxMutexGuiEnter();
        handler->SetInfo( wxT("All done.") );
        handler->playWater->Enable();
        wxMutexGuiLeave();
        handler->ready = true;
    }
    else {
        std::string log = ReadFileReformat( "changelog" );
       // std::cout << s << std::endl;
        wxMutexGuiEnter();
        handler->SetChangelog( GetWXString( log ) );
        handler->SetInfo( GetWXString(std::string( "Game is up to date (" + version + ")" )) );
        wxMutexGuiLeave();
    }
    f = fopen( "bin.zip", "rb" );
    if( f ) {
        fclose(f);
        remove( "bin.zip" );
    }
    wxMutexGuiEnter();
    handler->checkForUpdates->Enable();
    wxMutexGuiLeave();
    return NULL;
}

BEGIN_EVENT_TABLE(MyFrame,wxFrame)
    EVT_MENU(wxID_EXIT, MyFrame::OnQuit)
    EVT_CLOSE(MyFrame::OnCloseWindow)
    EVT_BUTTON(ID_PlayWater, MyFrame::OnPlayWater)
    EVT_BUTTON(ID_CheckForUpdates, MyFrame::OnCheckForUpdates )
    EVT_BUTTON(ID_Exit, MyFrame::OnQuit )
END_EVENT_TABLE()

MyFrame::MyFrame( wxWindow *parent, wxWindowID id, const wxString &title,
    const wxPoint &position, const wxSize& size, long style ) :
    wxFrame( parent, id, title, position, size, style ), ready(false)
{
    SetIcon( wxICON(wxICON_AAA) );
    curl_global_init(CURL_GLOBAL_WIN32);
    curl = curl_easy_init();
    curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, Writer );
    curl_easy_setopt( curl, CURLOPT_READFUNCTION, Reader );
    curl_easy_setopt( curl, CURLOPT_NOPROGRESS, 0L );
    curl_easy_setopt( curl, CURLOPT_PROGRESSFUNCTION, Progress );




    wxPanel* panel = new wxPanel( this, -1 );
    vertLayout = new wxBoxSizer( wxVERTICAL );

    wxStaticText* label1 = new wxStaticText( panel, -1, L"Changelog", wxDefaultPosition, wxDefaultSize );
    info = new wxStaticText( panel, -1, L"Info", wxDefaultPosition, wxDefaultSize );
    changelog = new wxTextCtrl( panel, -1, wxEmptyString, wxDefaultPosition, wxSize( -1, 500 ), wxTE_READONLY | wxTE_MULTILINE );

    std::ifstream is( "version" );
    std::string version;
    if( is.is_open() ) {
        is >> version;
        SetInfo( GetWXString( "Current version is " + version ) );
        is.close();
    }
    else {
        SetInfo( GetWXString( "There is no version of the game installed (apparently)" ) );
    }

    progress = new wxGauge( panel, -1, 0, wxDefaultPosition, wxSize( 600, -1 ) );
    progress->SetRange( 100 );
    curl_easy_setopt( curl, CURLOPT_PROGRESSDATA, progress );


    wxBoxSizer* buttons = new wxBoxSizer( wxHORIZONTAL );
    playWater = new wxButton( panel, ID_PlayWater, wxT( "Play Water" ), wxDefaultPosition, wxSize( -1, 50 ) );
    checkForUpdates = new wxButton( panel, ID_CheckForUpdates, wxT( "Check For Updates" ), wxDefaultPosition, wxSize( -1, 50 ) );
    wxButton* exit = new wxButton( panel, ID_Exit, wxT( "Exit" ), wxDefaultPosition, wxSize( -1, 50 ) );

    buttons->Add( playWater, 1, wxALIGN_CENTER_VERTICAL | wxEXPAND | wxRIGHT, 20 );
    buttons->Add( checkForUpdates, 1, wxALIGN_CENTER_VERTICAL | wxEXPAND );
    buttons->Add( exit, 1, wxALIGN_CENTER_VERTICAL | wxEXPAND | wxLEFT, 20 );

    //  Changelog Label
    vertLayout->Add( label1, 0, wxALIGN_CENTER_HORIZONTAL | wxTOP, 3 );
    vertLayout->Add( NULL, 10 );
    //  Text control for changelog
    vertLayout->Add( changelog, 0, wxEXPAND | wxLEFT | wxRIGHT, 60 );
    vertLayout->Add( NULL, 5 );
    //  Info
    vertLayout->Add( info, 0, wxALIGN_CENTER_HORIZONTAL );
    vertLayout->Add( NULL, 5 );
    //  Progress bar
    vertLayout->Add( progress, 0, wxALIGN_CENTER_HORIZONTAL );
    vertLayout->Add( NULL, 10 );
    //  Buttons
    vertLayout->Add( buttons, 0, wxEXPAND | wxLEFT | wxRIGHT, 50 );
    panel->SetSizer( vertLayout );


    FILE* f = NULL;
    f = fopen( "Water.exe", "r" );
    if( !f ) {
        playWater->Disable();
    }
    else {
        ready = true;
        fclose(f);
    }
}

void MyFrame::SetRange( int val ) {
    progress->SetRange( val );
}

void MyFrame::SetProgress( int val ) {
    progress->SetValue( val );
}

void MyFrame::SetInfo( const wxString& msg ) {
    info->SetLabel( msg );
    vertLayout->Layout();
}

void MyFrame::SetChangelog( const wxString& msg ) {
    changelog->ChangeValue( msg );
}

void MyFrame::OnQuit( wxCommandEvent &event ) {
    if( thread ) {
        thread->Delete();
        thread = NULL;
    }
    delete info;
    delete progress;
    curl_easy_cleanup( curl );
    Close( true );
}

void MyFrame::OnCloseWindow( wxCloseEvent &event ) {
    Destroy();
}

void MyFrame::OnPlayWater( wxCommandEvent& event ) {
    if( ready ) {
        system( "call start Water.exe" );
        wxCommandEvent evt(wxEVT_CLOSE_WINDOW, wxID_EXIT);
        wxPostEvent( this, evt );
    }
}

void MyFrame::OnCheckForUpdates( wxCommandEvent& event ) {
    checkForUpdates->Disable();
    if( !thread ) {
        thread = new DownloadThread( this );
        if( thread->Create() != wxTHREAD_NO_ERROR ) {
            std::cout << "Can't create the download thread !" << std::endl;
            delete thread;
            thread = NULL;
        }
    }
    if( thread->Run() != wxTHREAD_NO_ERROR ) {
        std::cout << "Can't run the thread !" << std::endl;
        delete thread;
        thread = NULL;
    }
}



IMPLEMENT_APP(MyApp)

MyApp::MyApp() {

}

bool MyApp::OnInit() {
    MyFrame *frame = new MyFrame( NULL, -1, wxT("Water Launcher"), wxPoint( 70, 20 ), wxSize(800, 700) );
    frame->SetMinSize( wxSize( 800, 700 ) );
    frame->SetMaxSize( wxSize( 800, 700 ) );
    frame->Show( true );
    
    return true;
}


