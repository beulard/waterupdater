Water Updater project by Beulard :
===============================================
Matthias 'Beulard' Dubouchet <beulard@gmail.com>

-------------

Tools used :
------------
* C++
* QtCreator 2.6.2
* Qt libraries 5.0.1
* MinGW with gcc/g++ 4.7.2

Libraries used :
----------------
* wxWidgets 2.8.12
* cURL 7.29.0 and its dependencies
