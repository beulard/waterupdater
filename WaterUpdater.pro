TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR = bin
OBJECTS_DIR = obj

DEFINES += CURL_STATICLIB
LIBS += -L$$PWD/lib -lcurl -lwxbase28u_net -lwxbase28u -lwxmsw28u_core -lwxmsw28u -lz -lwinspool -lole32 -lwsock32 -lcomctl32 -loleaut32 -luuid -lwldap32 -mwindows -static -static-libgcc -static-libstdc++

INCLUDEPATH += $$PWD/include

win32:RC_FILE = include/resources.rc

SOURCES += \
    src/WaterUpdater.cpp

HEADERS += \
    src/WaterUpdater.hpp

OTHER_FILES += \
    include/resources.rc
